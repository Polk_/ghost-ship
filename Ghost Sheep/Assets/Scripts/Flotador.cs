using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flotador : MonoBehaviour
{
    public float profundidadAntesDeSumergirse = 0.5f;
    public float cantidadDeDesplazamiento = 3f;
    public Rigidbody rig;

    private void FixedUpdate()
    {
        float waveHeight = WaveManager.instance.GetWaveHeight(transform.position.x);
        if (transform.position.y < waveHeight)
        {
            float multiplicadorDeDesplazamiento = Mathf.Clamp01((waveHeight - transform.position.y) / profundidadAntesDeSumergirse) * cantidadDeDesplazamiento;
            rig.AddForce(new Vector3(0f, Mathf.Abs(Physics.gravity.y) * multiplicadorDeDesplazamiento, 0f), ForceMode.Acceleration);

        }





    }
}
