using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public static WaveManager instance;

    public float amplitud = 1f;
    public float longitud = 2f;
    public float velocidad = 1f;
    public float offset = 0f;


   private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exist, destroying objetc");
            Destroy(this);
        }



    }

    private void Update()
    {
        offset += Time.deltaTime * velocidad; 
    }

    public float GetWaveHeight(float _x)
    {
        return amplitud * Mathf.Sin(_x / longitud + offset);
    }
}
