using UnityEngine;

public class ColorDePlano : MonoBehaviour
{
    public Color colorA;
    public Color colorB;
    [SerializeField] private Color colorBase;
    private Renderer _renderer;
    private bool isColorA;

    void Start()
    {
        _renderer = GetComponent<Renderer>();
        _renderer.material.color = colorBase; 
        isColorA = false; 
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
        {
            CambiarColor();
        }
    }

    void CambiarColor()
    {
        if (isColorA)
        {
            _renderer.material.color = colorB;
            isColorA = false;
        }
        else
        {
            _renderer.material.color = colorA;
            isColorA = true;
        }
    }
}

