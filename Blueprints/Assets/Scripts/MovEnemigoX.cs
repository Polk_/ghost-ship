using UnityEngine;

public class MovEnemigoX : MonoBehaviour
{
    
    public float velocidadX = 2.0f;

    
    public float limiteIzquierdo = -5.0f;
    public float limiteDerecho = 5.0f;

  
    private int direccion = 1;

    
    void Update()
    {
        
        float desplazamientoX = velocidadX * direccion * Time.deltaTime;

        
        transform.Translate(Vector3.right * desplazamientoX);

       
        if (transform.position.x >= limiteDerecho)
        {
            direccion = -1;
        }
        else if (transform.position.x <= limiteIzquierdo)
        {
            direccion = 1;
        }
    }
}
