using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoPlano : MonoBehaviour
{
    private AudioSource audioSource;

    void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
    }

   
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            audioSource.Play();
        }
          
    }
}
