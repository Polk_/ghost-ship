using UnityEngine;

public class MovPlataforma : MonoBehaviour
{
  
    public float velocidad = 2.0f;

  
    public float alturaMaxima = 4.0f;
    public float alturaMinima = 0.0f;

    
    private int direccion = 1;

    
    void Update()
    {
       
        float desplazamiento = velocidad * direccion * Time.deltaTime;

       
        transform.Translate(Vector3.up * desplazamiento);

        
        if (transform.position.y >= alturaMaxima)
        {
            direccion = -1; 
        }
        else if (transform.position.y <= alturaMinima)
        {
            direccion = 1; 
        }
    }
}
