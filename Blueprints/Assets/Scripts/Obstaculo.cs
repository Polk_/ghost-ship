using UnityEngine;

public class Obstaculo : MonoBehaviour
{
    public GameObject canvasPerdido;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Laser"))
        {
           
            if (canvasPerdido != null)
            {
                canvasPerdido.SetActive(true);
           
                GetComponent < MovPlayer>().enabled = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Laser"))
        {
          
            if (canvasPerdido != null)
            {
                canvasPerdido.SetActive(true);
                
                GetComponent < MovPlayer>().enabled = false;
            }
        }
    }
}
