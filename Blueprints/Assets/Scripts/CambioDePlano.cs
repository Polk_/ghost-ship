using UnityEngine;

public class CambioDePlano : MonoBehaviour
{
    public float distanciaTraslacion = 1.0f;
    private bool trasladado = false;
   

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {

            if (trasladado)
            {

                transform.Translate(Vector3.up * -distanciaTraslacion);
            }
            else
            {

                transform.Translate(Vector3.up * distanciaTraslacion);
            }


            trasladado = !trasladado;
        }
    }
}
